#!/bin/bash

# avoids running twice https://gitlab.com/gitlab-org/gitlab-runner/issues/1380#note_120576018
if [ -f /ran.txt ]; then
  exit 0
fi
touch /ran.txt

python /usr/src/app/cli.py -d src -o static-code-security-analysis.json

cat static-code-security-analysis.json
